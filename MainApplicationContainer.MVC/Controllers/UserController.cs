﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PromotionItems
{
    public class UserController : ApiController
    {
        public class CurrentUser
        {
            public string UserName
            { get; set; }

            public string dcShort
            { get; set; }

            //public string givenName
            //{ get; set; }
        }
        // GET: api/User
        public CurrentUser Get()
        {
            CurrentUser cu = new CurrentUser();
            var identity = User.Identity;
            cu.UserName = identity.Name.ToString().Remove(0,5);
            if (cu.UserName == "Clyral")
            {
                cu.dcShort = "CO";
            }
            else
            {
                cu.dcShort = "NA";
            }
            //cu.givenName = "Emlyn";
            return cu;
        }

        // GET: api/User/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/User
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/User/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/User/5
        public void Delete(int id)
        {
        }
    }
}
