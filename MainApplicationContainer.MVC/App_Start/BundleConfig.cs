﻿using System.Web;
using System.Web.Optimization;

namespace MainApplicationContainer.MVC
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new Bundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new Bundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/select.css",
                      "~/Content/site.css"
                      ));

            bundles.Add(new StyleBundle("~/bundles/appCssBundle").Include(
                      "~/Content/sweetalert.css",
                      "~/Content/loading-bar.css",
                      "~/Content/ui-grid.css",
                      "~/app/common/app.css"
                      ).Include("~/Content/font-awesome.css", new CssRewriteUrlTransform()));

            bundles.Add(new ScriptBundle("~/bundles/appCore/scripts").Include(
                "~/Scripts/angular.min.js",
                "~/Scripts/angular-resource.js",
                "~/Scripts/angular-ui-router.js",
                "~/Scripts/angular-ui/ui-bootstrap.js",
                "~/Scripts/angular-ui/ui-bootstrap-tpls.js",
                "~/Scripts/angular-messages.js", "~/Scripts/angular-cookies.js",
                "~/Scripts/angular-animate.js",
                "~/Scripts/spin.js",
                "~/Scripts/angular-spinner.js",
                "~/Scripts/ui-grid.js",
                "~/Scripts/angular-sanitize.js",
                "~/Scripts/select.js",

                "~/Scripts/loading-bar.js",
                "~/Scripts/lodash.js",
                "~/Scripts/sweetalert-dev.js",


                 "~/app/common/appCommon.js",
                "~/app/common/modal/modal.js",

               "~/app/common/alerts/alertsService.js",
                "~/app/common/alerts/alertsDirective.js",

                "~/app/common/enterKeyDirective.js",
                 "~/app/common/userService.js",

                "~/app/common/formInputDirective.js",

                "~/app/common/exceptionHandler.js",

                "~/app/common/spUtility.js",
                "~/app/appCore/appCore.js"


            ));



            bundles.Add(new Bundle("~/bundles/app/minTest").Include(

             ));


            bundles.Add(new ScriptBundle("~/bundles/app/scripts").Include(

                  "~/app/app.js",
                  "~/app/home/home.module.js",
                  "~/app/home/homeController.js",

                  "~/app/vendorData/vendorData.module.js",
                  "~/app/vendorData/vendorData.ctrl.js",
                  "~/app/vendorData/viewVendors.ctrl.js",
                  "~/app/vendorData/errorDetectionLog.ctrl.js",
                  "~/app/vendorData/vendorDataService.js",

                  "~/app/sapCoDetails/sapCoDetails.module.js",
                  "~/app/sapCoDetails/sapCoDetails.ctrl.js"


             ));

        }

    }
}
