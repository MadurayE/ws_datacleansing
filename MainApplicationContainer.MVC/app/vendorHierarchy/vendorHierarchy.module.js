﻿(function () {
    'use strict';

    angular.module('vendorHierarchy', [
        // Angular modules 

        // Custom modules 
        "appConfig"

        // 3rd Party Modules
        
    ])

    .config(["$stateProvider", "$urlRouterProvider", "$locationProvider", vendorHierarchyConfig])

    function vendorHierarchyConfig($stateProvider, $urlRouterProvider, $locationProvider) {
        $stateProvider
        .state('vendorGroups',
         {
             url: "/vendorGroups",
             templateUrl: "app/vendorHierarchy/vendorGroups.html",
             controller:"vendorGroupsController as vm"
         })
        .state('vendorDivisions',
        {
            url: "/vendorDivisions",
            templateUrl: "app/vendorHierarchy/vendorDivisions.html",
            controller: "vendorDivisionsController as vm"
        })
        .state('vendorHierarchy',
        {
            url: "/vendorHierarchy",
            templateUrl: "app/vendorHierarchy/vendorHierarchy.html",
            controller:"vendorHierarchyController as vm"
        })
    }
})();