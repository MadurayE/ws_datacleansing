﻿(function () {
    return;
    angular
        .module("compositeItemTypes",
                            ["common.services","common.directives", "ui.router", "ui.bootstrap", "ngMessages","angular-loading-bar","ngCookies"])


        .config(["$stateProvider", "$urlRouterProvider", "$httpProvider", function ($stateProvider, $urlRouterProvider, $httpProvider) {  //router for main app
            $stateProvider
                .state("compositeItemTypes", {                  //show all composite item types
                    url: "/compositeItemTypes",
                    templateUrl: "app/compositeItemTypes/views/compositeItemTypesView.html",
                    controller: "CompositeItemTypesController as vm"
                })

                .state("compositeItemTypeEdit", {               // edit composite item types
                    url: "/compositeItemTypeEdit/:compositeItemTypeId",
                    templateUrl: "app/compositeItemTypes/views/compositeItemTypeEdit.html",
                    controller: "CompositeItemTypeEditController as vm",
                    resolve: {
                        compositeItemTypesService: "compositeItemTypesService",
                        compositeItemType:["compositeItemTypesService","$stateParams",
                                function (compositeItemTypesService, $stateParams) {
                            return compositeItemTypesService.getCompositeItemType($stateParams.compositeItemTypeId);
                        }],
                        rewardsService: "rewardsService",
                        rewards: ["rewardsService", "openModal", function (rewardsService, openModal) {   //rewards lookup list data
                            return rewardsService.query(function (data) { }, function (error) {
                                openModal.swalErr("Oops, could not get Rewards", error.statusText, error.data.message);
                            }).$promise;
                        }],
                        conditionsService: "conditionsService",
                        conditions: ["conditionsService",function (conditionsService) { //conditions lookup list data
                            return conditionsService.query(function (data) { }, function (error) {
                                openModal.swalErr("Oops, could not get Conditions", error.statusText, error.data.message);
                            }).$promise;
                        }]
                    }

                });

            //$locationProvider.html5Mode(true);

        }]);


    app.run(["userService", "$cookies", "$rootScope","openModal",
        function (userService, $cookies, $rootScope, openModal) {
            //get user's login name in order to make AD service call
            //userService.userName.get(function (data) {
            //    $cookies.put('currentUserName', data.UserName);
            //}, function (error) {
            //    swal("Oops!", "Could not retrieve current user's login name", "error");
            //    openModal.swalErr("Oops, could not get user name", error.statusText, error.data.message);
            //});

            //global function to scroll to top of page when new route is loaded
            $rootScope.$on('$stateChangeSuccess', function () {
                document.body.scrollTop = document.documentElement.scrollTop = 0;
            });
        }

    ]);

}());