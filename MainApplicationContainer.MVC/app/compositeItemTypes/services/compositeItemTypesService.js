﻿(function (){
    "use strict";

    angular
        .module("common.services")
        .factory("compositeItemTypesService",["$resource","appConfig","openModal",compositeItemTypesService])


    function compositeItemTypesService($resource, appConfig, openModal) {

        //property
        var citResource = $resource(appConfig.SERVERPATH + "api/compositeItemTypes/:id", null,
                {
                    'update': { method: 'PUT', withCredentials: true },
                    'getAllItems': { method: 'get', isArray: true, withCredentials: true },
                    'query': { method: 'get', isArray: true, withCredentials: true },
                    'get': { method: 'get', withCredentials: true },
                    'options': { method: 'options', withCredentials: true }
                });

        function getDefaultItemType(){ 
            var newType = {
                "activeCode": "Y",
                "compItemTypeCode": "",
                "compItemTypeId": 0,
                "compItemTypeName": "",
                "condition": 1,
                "conditionDescription": "Buy minimum of x",
                "dateTimeDependent": "N",
                "helpHint": "",
                "maxSubGroups": 1,
                "minSubGroups": 1,
                "reward": 1,
                "rewardDescription": "Total Price",
                "subDepartments": "N"
            };
            return newType;
        }

        //method
        function getCompositeItemType(id)
        {
            return citResource.get({ id: id }).$promise.then(
                null,  
                function (error) {
                    openModal.swalErr("Oops, could not fetch item", error.statusText, error.data.message);
                    //add logging?
                });
        }

        //public
        return {
                citDataResource: citResource,
                getCompositeItemType: getCompositeItemType,
                getDefaultItemType: getDefaultItemType

        }
    }


}());