﻿(function () {

    "use strict";

    angular
        .module("common.services")
        .factory("rewardsService",
        ["$resource",
         "appConfig",
         RewardsService
        ])

    function RewardsService($resource, appConfig) {
        return $resource(appConfig.SERVERPATH + "api/rewards/:id"
            );
    }
}());