﻿(function () {

    "use strict";

    angular
        .module("common.services")
        .factory("conditionsService",
        ["$resource",
         "appConfig",
         ConditionsService
        ])

    function ConditionsService($resource, appConfig) {
        return $resource(appConfig.SERVERPATH + "api/conditions/:id"
            );
    }
}());