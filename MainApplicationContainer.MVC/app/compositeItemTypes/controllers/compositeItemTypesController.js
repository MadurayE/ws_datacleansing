﻿(function () {
    "use strict";

    angular
        .module("app")
        .controller("CompositeItemTypesController",
                        ["compositeItemTypesService","openModal","$state",CompositeItemTypesController])

    function CompositeItemTypesController(compositeItemTypesService,openModal,$state) {
        var vm = this;
        vm.showInactive = false;

        compositeItemTypesService.citDataResource.getAllItems({showInactive: true},function (data) {
            vm.compositeItemTypes = data;
        }, function (response) {
            openModal.swalErr("Oops, could not fetch data", error.statusText, error.data.message);
        });

        vm.showCompItemTypeEdit = function (id) {
            $state.go("compositeItemTypeEdit",{compositeItemTypeId:id})
        }
    }
}());