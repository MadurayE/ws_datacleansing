﻿(function () {

    "use strict";

    angular
        .module("app")
        .controller("CompositeItemTypeEditController",
                    ["compositeItemType", "rewards", "conditions", "openModal", "$state","alertsService", CompositeItemTypeEditController])

    function CompositeItemTypeEditController(compositeItemType, rewards, conditions, openModal, $state, alertsService) {
        var vm = this;
        //fill model

        vm.compositeItemType = compositeItemType;
        vm.originalObject = angular.copy(compositeItemType);
        vm.rewards = rewards;
        vm.conditions = conditions;

        if (compositeItemType.compItemTypeId === 0)
        { vm.title = "Add Composite Item Type"; }
        else
        {
            vm.title = "Edit Composite Item Type";
        }
 
        //form sumitted handler
        vm.submit = function () {
            openModal.confirm("Save Composite Item Type",vm.compositeItemType,"Are you sure you wish to save changes to this Item Type").then(saveItem);
        }

        //form cancelled handler
        vm.cancel = function (editForm) {
            editForm.$setPristine();
            vm.compositeItemType = angular.copy(vm.originalObject);
            $state.transitionTo("compositeItemTypes", {arg:'arg'});
        }


        //save the item after user confirms
        var saveItem = function(cit){
            if (cit.compItemTypeId) 
            {
                cit.$update({ id: cit.compItemTypeId },
                    function (data) 
                    {
                        
                        $state.transitionTo("compositeItemTypes", { arg: 'arg' });
                        swal("Composite Item Type", "Save was successful", "success");
                        //alertsService.raiseSuccess("Composite Item Type saved"); 
                        
                    }, function (error) {
                       openModal.swalErr("Oops, could not update", error.statusText, error.data.message);
                    })
            }
            else 
            {
                cit.$save(
                    function (data) {
                        vm.originalObject = angular.copy(data);
                        $state.transitionTo("compositeItemTypes", { arg: 'arg' });
                        swal("Composite Item Type", "Record was added successfully", "success");
                        //alertsService.raiseSuccess("New Composite Item Type successfully added");
                    }, function (error) {
                        openModal.swalErr("Oops, could not save", error.statusText, error.data.message);
                    }
                    )
            }
        }
    }

}());