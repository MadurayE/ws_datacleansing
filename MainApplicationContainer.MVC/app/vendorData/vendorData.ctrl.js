﻿(function () {
    'use strict';

    angular
        .module('vendorData')
        .controller('vendorDataController', vendorDataController);

    vendorDataController.$inject = ['$scope', '$state', 'vendorDataService'];

    function vendorDataController($scope, $state, vendorDataService) {
        /* jshint validthis:true */
        var vm = this;
        vm.title = "View/Maintain Vendor Data";
        $scope.$watch(function () { return vendorDataService.getMainVendorTitle(); }, function (newVal, oldVal) {
            if (oldVal !== newVal) {
                vm.title = newVal;
            }
        });

        activate();
        
        function activate() { }

        vm.changeTabTo = function(state){
            $state.transitionTo(state);
        }
    }
})();
