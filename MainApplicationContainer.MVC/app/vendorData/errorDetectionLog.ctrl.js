﻿(function () {
    'use strict';

    angular
        .module('vendorData')
        .controller('errorDetectionLogController', errorDetectionLogController);

    errorDetectionLogController.$inject = ['vendorDataService']; 

    function errorDetectionLogController(vendorDataService) {
        /* jshint validthis:true */
        var vm = this;
        vendorDataService.setMainVendorTitle('Error Detection Log');

        activate();

        function activate() { }
    }
})();
