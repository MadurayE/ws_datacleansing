﻿(function () {
    'use strict';

    angular.module('vendorData', [
        // Angular modules 
        "ngSanitize", "ui.select",
        // Custom modules 
        "appCore"
        // 3rd Party Modules

    ])


    .config(["$stateProvider", "$urlRouterProvider", "$locationProvider", vendorDataConfig]);

    function vendorDataConfig($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) {  //router for vendor data
        $stateProvider
        .state("vendorData",
        {
            url: "/vendorData",
            abstract:true,
            templateUrl: "app/vendorData/vendorData.html",
            controller: "vendorDataController as vm",
            resolve: {
            }
        })
        .state("viewVendors", {
            parent: "vendorData",
            url:"/viewVendors",
            templateUrl: "app/vendorData/viewVendors.html",
            controller: "viewVendorsController as vm"
        })
        .state("errorDetectionLog", {
            parent: "vendorData",
            url:"/errorDetectionLog",
            templateUrl: "app/vendorData/errorDetectionLog.html",
            controller: "errorDetectionLogController as vm"

        });
        //$locationProvider.html5Mode(true);

    }
})();