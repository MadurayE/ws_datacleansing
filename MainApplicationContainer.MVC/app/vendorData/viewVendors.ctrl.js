﻿(function () {
    'use strict';

    angular
        .module('vendorData')
        .controller('viewVendorsController', viewVendorsController);

    viewVendorsController.$inject = ['vendorDataService'];

    function viewVendorsController(vendorDataService) {
        /* jshint validthis:true */
        var vm = this;
        vendorDataService.setMainVendorTitle('View/Maintain Vendor Data');
        vm.clearParams = clearSearchParameters;
        vm.dc = "";
        vm.vatNo = "";
        vm.vendorName = "";
        vm.coRegNo = "";
        vm.division = "";
        vm.bankAccNo = "";
        vm.primEanCode = "";
        vm.vendors = vendorDataService.fakeVendorList;



        activate();

        function activate() {
            vm.gridOptions = vendorDataService.getGridOptions;
               
            vendorDataService.getData().$promise.then(function(data){
                vm.gridOptions.data = data;
                //vm.vendors = data;
            }
            );
            
        }

      function clearSearchParameters() {
            vm.dc = "";
            vm.vatNo = "";
            vm.vendorName = "";
            vm.coRegNo = "";
            vm.division = "";
            vm.bankAccNo = "";
            vm.primEanCode = "";
            vm.vendors.selected = undefined;
        }
    }
})();
