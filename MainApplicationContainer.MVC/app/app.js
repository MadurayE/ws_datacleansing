﻿
(function () {

    var app = angular.module("app",
                            //["compositeItemTypes",
                                ["appCore", "home", "vendorData", "sapCoDetails"]);

    app.config([ "$urlRouterProvider","$stateProvider", "$httpProvider", "$locationProvider", appRouter]);

    function appRouter( $urlRouterProvider,$stateProvider, $httpProvider, $locationProvider) {  //router for main app
     
        $httpProvider.defaults.withCredentials = true;
        //$locationProvider.html5Mode(true);
        $urlRouterProvider.otherwise("home");
        $stateProvider

            .state("unauthorised", {                  //No permissions - Unauthorised
                url: "/accessdenied",
                templateUrl: "app/common/templates/unauthorised.html"
            })

        //    .state("compositeItems", {                  //show all composite items
        //        url: "/compositeItems",
        //        templateUrl: "app/compositeItems/views/compositeItemsView.html",
        //        controller: "CompositeItemsController as vm"                
        //    })

        //     .state("compositeItemDetailsEdit", {                  //show composite item and details for edit
        //         url: "/compositeItemDetails/:compositeItemCode",           //:id
        //         templateUrl: "app/compositeItems/views/compositeItemDetailsEditView.html",
        //         controller: "CompositeItemDetailsEditController as vm",
        //         //params: { item: null },                 //should refactor to be restful
        //         resolve: {
        //             compositeItemsService: "CompositeItemsService",
        //             userService: "userService",
        //             selectedCompositeItem: ["CompositeItemsService", "$stateParams", "openModal", "userService", function (compositeItemsService, $stateParams, openModal, userService) {
        //                 //get composite item of selected row.
        //                 return compositeItemsService.dataService.get({ id: $stateParams.compositeItemCode, userdc: userService.dcShortCodeCookie() },
        //                      function (data) {
        //                      }, function (error) {
        //                          openModal.swalErr("Oops, could not fetch item", error.statusText, error.data.message);
        //                      }).$promise;
        //             }],
        //             editableProperties: ["CompositeItemsService", "$stateParams", "openModal", "userService", function (compositeItemsService, $stateParams, openModal, userService) { //editableProperties say whether the item header or details are editable
        //                 var dc = userService.dcShortCodeCookie();
        //                 return compositeItemsService.itemEditProperties.get({ id: $stateParams.compositeItemCode, bu: dc },
        //                         function (data) { }
        //                         , function (error) {
        //                             openModal.swalErr("Oops, could not check edit status", error.statusText, error.data.message);
        //                         }).$promise;
        //             }],
        //             compositeItemTypesService: "compositeItemTypesService",
        //             compositeItemTypes: ["compositeItemTypesService", "$stateParams", function (compositeItemTypesService, $stateParams) {
        //                 return compositeItemTypesService.citDataResource.getAllItems({ showInactive: false }, function (data) {
        //                     //if ($stateParams.cItem.compositeItemType.activeCode === "N") {
        //                     //    data.push($stateParams.cItem.compositeItemType);
        //                     //}
        //                 }, function (error) {
        //                     openModal.swalErr("Oops, could not get Composite Types", error.statusText, error.data.message);
        //                 }).$promise;
        //             }],
        //             categoryLookupService: "categoryLookupService",
        //             planningCategories: ["categoryLookupService", "userService", function (categoryLookupService, userService) {
        //                 var dc = userService.dcShortCodeCookie();
        //                 return categoryLookupService.planningCategoryLookup.query({ bu: dc }, function (data) { }, function (error) {
        //                     openModal.swalErr("Oops, could not get Planning Categories", error.statusText, error.data.message);
        //                 }).$promise;
        //             }],
        //             retailCategories: ["categoryLookupService", function (categoryLookupService) {
        //                 return categoryLookupService.retailCategoryLookup.query(function (data) { }, function (error) {
        //                     openModal.swalErr("Oops, could not get Retail Categories", error.statusText, error.data.message);
        //                 }).$promise;
        //             }]
        //         }
        //     })

        //    .state("compositeItemTypes", {                  //show all composite item types
        //        url: "/compositeItemTypes",
        //        templateUrl: "app/compositeItemTypes/views/compositeItemTypesView.html",
        //        controller: "CompositeItemTypesController as vm"
        //    })

        //    .state("compositeItemTypeEdit", {               // edit composite item types
        //        url: "/compositeItemTypeEdit/:compositeItemTypeId",
        //        templateUrl: "app/compositeItemTypes/views/compositeItemTypeEdit.html",
        //        controller: "CompositeItemTypeEditController as vm",
        //        resolve: {
        //            compositeItemTypesService: "compositeItemTypesService",
        //            compositeItemType: ["compositeItemTypesService", "$stateParams",
        //                    function (compositeItemTypesService, $stateParams) {
        //                        return compositeItemTypesService.getCompositeItemType($stateParams.compositeItemTypeId);
        //                    }],
        //            rewardsService: "rewardsService",
        //            rewards: ["rewardsService", "openModal", function (rewardsService, openModal) {   //rewards lookup list data
        //                return rewardsService.query(function (data) { }, function (error) {
        //                    openModal.swalErr("Oops, could not get Rewards", error.statusText, error.data.message);
        //                }).$promise;
        //            }],
        //            conditionsService: "conditionsService",
        //            conditions: ["conditionsService", function (conditionsService) { //conditions lookup list data
        //                return conditionsService.query(function (data) { }, function (error) {
        //                    openModal.swalErr("Oops, could not get Conditions", error.statusText, error.data.message);
        //                }).$promise;
        //            }]
        //        }

        //    })

       
    };

    app.controller("main", [function () {
        //alert(uname);
    }]);

    //app.run(["userService", "$cookies", "$rootScope", "openModal", "$state",
    //    function (userService, $cookies, $rootScope, openModal, $state) {
    //        //get user's login name 
    //        userService.userName.get(function (data) {
    //            $cookies.put('currentUserName', data.UserName);
    //        }, function (error) {
    //            //swal("Oops!", "Could not retrieve current user's login name", "error");
    //            openModal.swalErr("Oops, could not get user name", error.statusText, error.data.message);
    //        });

    //        //global function to scroll to top of page when new route is loaded
    //        $rootScope.$on('$stateChangeSuccess', function () {
    //            document.body.scrollTop = document.documentElement.scrollTop = 0;
    //        });

    //        $rootScope.$on('$stateChangeStart',
    //            function (event, toState, toParams, fromState, fromParams, options) {
    //                var toHome = toState.name === "home";
    //                var toUnauthorised = toState.name === "unauthorised";
    //                var auth = $cookies.get('authorisedForApp');
    //                if (!toHome && !toUnauthorised) {               //dont like this - ugly code
    //                    if (auth) {
    //                        if (auth === "true") {
    //                            var context = $cookies.get('dcShortCode');
    //                            if (!context) {
    //                                event.preventDefault();
    //                                $state.go("home");
    //                            }
    //                        } else {
    //                            //redirect to Not Authorised page
    //                            event.preventDefault();
    //                            $state.go("unauthorised");
    //                        }
    //                    } else {
    //                        event.preventDefault();
    //                        $state.go("home");
    //                    }

    //                } else {
    //                    if (toHome) {
    //                        if (auth) {
    //                            if (auth === "false") {
    //                                //redirect to Not Authorised page
    //                                event.preventDefault();
    //                                $state.go("unauthorised");
    //                            }
    //                        }
    //                    }
    //                }

    //            });
    //    }

    //]);

}());