﻿(function () {
    'use strict';

    angular
        .module('home', [
        // Angular modules 

        // Custom modules 

        // 3rd Party Modules
        
        ])

        .config(["$stateProvider", "$urlRouterProvider", "$locationProvider", homeConfig]);
        
    function homeConfig($stateProvider, $urlRouterProvider, $locationProvider) {  //router for main app
            $stateProvider
            .state("home",
            {                            //home page
                url: "/home",
                templateUrl: "app/home/home.html",
                controller: "homeController as vm",
                resolve: {
                    userService: "userService",
                    currentUser: ["userService", "$q", function (userService, $q) {
                        var deferred = $q.defer();
                        userService.userNameResource.get().$promise.then(function (data) {
                            deferred.resolve(data);
                        });
                        return deferred.promise;
                    }],
                    userAccount: ["userService", "currentUser", "$rootScope", "openModal", "$cookies", "$q", "$log","spUtility",
                        function (userService, currentUser, $rootScope, openModal, $cookies, $q, $log, spUtility) {
                            var ret = $q.defer();
                            currentUser.$promise.then(function (usrData) {
                                userService.userResource.get({ username: usrData.UserName , objectWeight : "core"}).$promise.then(function (data) {
                                    ret.resolve(data);
                                    if (data.dcShort===null){
                                        data.dcShort = usrData.dcShort;
                                    } 
                                    $rootScope.DC = data.dcShort;
                                    
                                }, function (error) {
                                    spUtility.handleServiceError(error, "Couldn't fetch user AD info");
                                    $q.reject(error);
                                });
                            });
                            return ret.promise;
                        }]
                //    ,userAuth: ["userService", "currentUser", "$q", function (userService, currentUser, $q) {
                //        var ret = $q.defer();
                //        currentUser.$promise.then(function (usrData) {
                //            userService.userAuthResource.get({ username: usrData.UserName }).$promise.then(function (data) {
                //                ret.resolve(data);
                //            }, function (error) {
                //                $q.reject(error);
                //            });
                //        })
                //        return ret.promise;
                //    }]
                }
            })

            //$locationProvider.html5Mode(true);

        }
})();