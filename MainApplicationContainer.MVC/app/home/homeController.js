﻿(function () {
    "use strict";

    angular
        .module("app")
        .controller("homeController", ["$rootScope","appSettings","userService", "userAccount", "$cookies",
            //, , "userAuth", 
            HomeController])

    function HomeController($rootScope, appSettings, userService, userAccount, $cookies
       // , , userAuth, 
        ) {
        var vm = this;
        $rootScope.targetServer = appSettings.targetServer;

        userAccount.$promise.then(function (data) {
            vm.user = data;
            vm.userName = userService.currentUserNameCookie;
            vm.isCOUser = vm.user.dcShort === "CO";
            $cookies.put('dcContext', vm.user.dcShort);
            userService.setDcShortCodeCookie(vm.user.dcShort);
            $rootScope.DC = $rootScope.DC
            //if (vm.isCOUser) {
            //    $rootScope.DC = $rootScope.DC + " as " + vm.targetDC;
            //}
        }, function (error) {
            swal("Couldn't Fetch User", "Error encountered when fetching user's AD info","error");
        });
        //userAuth.$promise.then(function (data) {
        //    $cookies.put('authorisedForApp', data.authorisedForApps === 'Y');
        //}, function (error) {
        //     swal("Couldn't Fetch User", "Error encountered when fetching user's AD info","error");
        //});

        vm.chooseDC = function () {
            //alert(vm.targetDC)
            userService.setDcShortCodeCookie(vm.targetDC);
            $rootScope.DC = $cookies.get('dcContext') + " as " + vm.targetDC;

        }
    }

})();