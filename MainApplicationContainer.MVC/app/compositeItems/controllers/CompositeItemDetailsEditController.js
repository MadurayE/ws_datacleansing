﻿(function () {

    "use strict";

    angular
        .module("app")
        .controller("CompositeItemDetailsEditController",
                    ["editableProperties", "$state", "$stateParams", "CompositeItemsService","selectedCompositeItem", "$log",
                    "userService", "openModal", "compositeItemTypesService", "compositeItemTypes", "alertsService", "planningCategories",
                    "retailCategories", "$scope", CompositeItemDetailsEditController])

    function CompositeItemDetailsEditController(editableProperties, $state, $stateParams, compositeItemsService,selectedCompositeItem, $log,
        userService, openModal, compositeItemTypesService, compositeItemTypes, alertsService, planningCategories, retailCategories, $scope) {

        //Init scope variables
        var vm = this;
        vm.compositeItemTypes = compositeItemTypes;
        selectedCompositeItem.$promise.then(function (data) {
            vm.compositeItem = selectedCompositeItem;
            if (vm.compositeItem.compositeItemType.activeCode === "N") {
                vm.previousCompositeType = angular.copy(vm.compositeItem.compositeItemType);
            }
            vm.selectedType = vm.compositeItem.compositeItemType;
            vm.originalCompositeItem = angular.copy($stateParams.item);
            if (vm.compositeItem.memberId === 0) {
                vm.title = "Add New Composite Item"
            } else {
                vm.title = "Edit Composite Item";
            }
            vm.updatePlanningCategory = vm.compositeItem.planningCategory === null;
        });

        //if (vm.compositeItem.compositeItemType.activeCode === "N") {
        //    vm.previousCompositeType = angular.copy(vm.compositeItem.compositeItemType);
        //}

        //vm.selectedType = vm.compositeItem.compositeItemType;
        //vm.originalCompositeItem = angular.copy($stateParams.item);

        //if (vm.compositeItem.memberId === 0) {
        //    vm.title = "Add New Composite Item"
        //} else {
        //    vm.title = "Edit Composite Item";
        //}

        vm.userDc = userService.dcShortCodeCookie();
        vm.canEditHeader = editableProperties.headerEditable === "Y";
        vm.canEditDetails = editableProperties.detailsEditable === "Y";
        vm.planningCategories = planningCategories;
        vm.retailCategories = retailCategories;


        //document ready function. Notify user if item or details are not editable
        angular.element(document).ready(function () {
            var msg = "";
            var msg2="";
            if (!vm.canEditHeader) { msg = "This item has been used in a current or past promotion. The header is not editable."; }
            if (!vm.canEditDetails) { msg2 = "The details are not editable because this Composite Item is being used in a current/future promotion."; }
            if (msg) { alertsService.raiseWarning(msg); }
            if (msg2) { alertsService.raiseWarning(msg2); }
        });

        // --Event handlers

        // Save clicked. Save composite item
        vm.saveClicked =
            function () {
                openModal.confirm("Save Composite Item", vm.compositeItem, "Are you sure you want to save this item and its details").then(saveItem);
            };

        var saveItem =
            function (item) {
                if (item.code) {
                    item.lastChgUserName = userService.getUserNameCookie();
                    item.$update({ id: item.code },
                    function (data) {
                        openModal.swalSuccess("Composite Item", data);
                        $state.transitionTo("compositeItems");
                    },
                    function (error) {
                        openModal.swalErr("Sorry, could not save item", error.statusText, error.data.message);
                    })
                } else {
                    item.enterUserName = userService.getUserNameCookie();
                    item.lastChgUserName = userService.getUserNameCookie();
                    item.$save({ id: item.code },
                    function (data) {
                        swal("Composite Item", "New Composite Item added successfully", "success");
                        $state.transitionTo("compositeItems");
                    },
                    function (error) {
                        // swal("Sorry, could not add item", "Error:" + error.statusText + "<br/>" + error.data.message, "error");
                        openModal.swalErr("Sorry, could not add item", error.statusText, error.data.message);
                    })
                }

            };


        //go back to composite item listing
        vm.cancelClicked =
            function () {
                alertsService.removeAllAlerts();
                $state.go("compositeItems"); //review to make restful
            };

        // Composite Item type changed - set non-model fields on composite item
        vm.compositeItemTypeChanged = function () {
            if (!vm.compositeItem.compositeItemType) {
                vm.compositeItem.compositeItemType = vm.previousCompositeType;
            }
            var selectedType = _.find(compositeItemTypes, ['compItemTypeId', vm.compositeItem.compositeItemType.compItemTypeId]);
            vm.compositeItem.compositeItemTypeCode = selectedType.compItemTypeCode;
            vm.compositeItem.compositeItemTypeId = selectedType.compItemTypeId;
            vm.compositeItem.compositeItemTypeName = selectedType.compItemTypeName;
            vm.compositeItem.reward = selectedType.reward;
            vm.compositeItem.condition = selectedType.condition;
        }

        // Planning Category dropdown select changed
        vm.updateHeaderProperties =
            function () {
                var selectedCategory = _.find(planningCategories, ['code', vm.compositeItem.planningCategory]);

                vm.compositeItem.headerProperties.code = vm.compositeItem.code;
                vm.compositeItem.headerProperties.planningCategoryCode = selectedCategory.code;
                vm.compositeItem.headerProperties.planningCategoryName = selectedCategory.name;
                vm.compositeItem.headerProperties.businessUnitCode = selectedCategory.businessUnitCode;
            }

        //Retail Category dropdown select changed
        vm.updateRetCatName = function () {
            var selectedCategory = _.find(retailCategories, ['code', vm.compositeItem.retailCategoryCode]);
            vm.compositeItem.retailCategoryName = selectedCategory.displayName;
        }

        // undo changes
        vm.undoClicked = function () {
            openModal.confirm("Undo Changes", vm.originalCompositeItem, "Are you sure you want to undo all your changes?").then(undoChanges);
        }

        var undoChanges = function (original) {
            vm.compositeItem = original;
            vm.originalCompositeItem = angular.copy(vm.compositeItem);
            $scope.$broadcast('originalCompItemRestored', original);
        }

    }

})();