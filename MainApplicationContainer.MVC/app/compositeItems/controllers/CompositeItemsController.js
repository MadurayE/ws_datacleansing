﻿    (function () {
    "use strict";

    angular
        .module("app")
        .controller("CompositeItemsController",
            ["CompositeItemsService", "openModal", "$state", "$uibModal", "appSettings", "$log", "userService", CompositeItemsController])


    function CompositeItemsController(compositeItemsService, openModal, $state, $uibModal, appSettings, $log, userService) {
        //initialise scope variables
        var vm = this;
        vm.displayInactive = false;
        vm.selectedRow = -1;
        vm.searchText = "";
        vm.itemSearch = "";
        vm.currentDc = userService.dcShortCodeCookie();

        //call get composite items service. Init compositeItems object
        compositeItemsService.dataService.query({ showInactive: vm.displayInactive }, function (data) {
            vm.compositeItems = data;
        })

        // ---- Event handlers

        //search button click
        vm.searchItems =
            function () {
                vm.compositeItems = null;

                compositeItemsService.dataService.searchFor({ name: vm.searchText, code: vm.itemSearch, showInactive: vm.displayInactive },
                    function (data) {
                        if (data.length === 0) {
                            swal(appSettings.unsucessfulSearchHdr, appSettings.unsuccessfulSearchMsg);
                        }
                        vm.compositeItems = data;
                    }, function (error) {
                        openModal.swalErr("Oops, could not search", error.statusText, error.data.message);
                    }
                    );

                vm.selectedRow = -1;
            };

        // table row click. Item selected to view/edit
        vm.showCompositeItemDetailsView =
            function (compositeItem, index) {
                vm.selectedRow = index;   //set selected row
                $state.go("compositeItemDetailsEdit", { compositeItemCode: compositeItem.code, cItem: compositeItem });

                //get composite item of selected row. Consider moving this to 'resolve' and to use id parameter to make more 'restful'
                //vm.selectedCompositeItem = compositeItemsService.dataService.get({ id: compositeItem.code, userdc: vm.currentDc },
                //    function (data) {


                //    }, function (error) {
                //        openModal.swalErr("Oops, could not fetch item", error.statusText, error.data.message);
                //    });

            }

        // Add new button clicked. Add new composite item. Call service to get back 'empty/default' composite item
        vm.addNew = function () {
            vm.selectedCompositeItem = compositeItemsService.dataService.get({ id: "0" }, function (data) {
                $state.go("compositeItemDetailsEdit", { item: data });

            }, function (error) {
                openModal.swalErr("Oops, could not initialise", error.statusText, error.data.message);
            });
        }

    }

})();