﻿(function(){
    "use strict";

    angular
        .module("app")
        .directive("subgroups", ["$log","userService","subgroupsService",subgroups])

    function subgroups($log, userService, subgroupsService) {
        return {
            restrict: 'AE',
            scope: {
                compositeItem: '=',
                userDc: '=',              //this is the DC context of the user or the context selected by a CO user
                detailsEditable:'='
            },
            replace:'true',
            templateUrl: "app/compositeItems/directives/views/subgroups.html",
            link: function (scope, elem, attr) {
                //initialise scope variables
                scope.compositeItem.$promise.then(function (data) {
                    scope.conditionDescription = data.compositeItemType.conditionDescription.replace("x", "");   
                    scope.rewardDescription = data.compositeItemType.rewardDescription;
                    scope.maxSubgroups = data.compositeItemType.maxSubGroups;
                    scope.maxSubgroupsUsed = scope.compositeItem.compositeItemGroups.length === scope.maxSubgroups;
                    scope.rewardCode = data.compositeItemType.reward;
                    scope.showDayTime = data.compositeItemType.dateTimeDependent === "Y";
                    if (data.dayTimeConditions.length === 0 && scope.showDayTime) {
                        
                        data.dayTimeConditions.splice(0,0, subgroupsService.getDefaultDaytimeObject(scope.compositeItem.code, userService.dcShortCodeCookie()));
                    }
                    scope.dayTimeConditions = data.dayTimeConditions[0];
                    scope.currentDetails = subgroupsService.getCurrentDetails(data, scope.userDc);
                  
                })
                scope.includeInactiveItems = false;
                scope.showOnlyMyDc = true;
                scope.excludeVariants = false;

                //watches
                scope.$watch(   //watch for new group being added
                            function (scope) {
                                if (scope.compositeItem.compositeItemGroups) {
                                    return scope.compositeItem.compositeItemGroups.length;
                                } else { return null; }
                            },
                            function(newVal, oldVal){
                                scope.maxSubgroupsUsed = newVal === scope.maxSubgroups;
                            }
                            );



                scope.$watch( //watch for change of composite item type
                    'compositeItem.compositeItemType'
                    , function (newVal, oldVal) {
                        if (newVal) {
                            scope.conditionDescription = newVal.conditionDescription.replace("x", "");
                            scope.rewardDescription = newVal.rewardDescription;
                            scope.rewardCode = newVal.reward;
                            scope.showDayTime = newVal.dateTimeDependent === "Y";
                            if (!scope.dayTimeConditions && scope.showDayTime) {
                                scope.compositeItem.dayTimeConditions.splice(0, 0, subgroupsService.getDefaultDaytimeObject(scope.compositeItem.code, userService.dcShortCodeCookie()));
                                scope.dayTimeConditions = scope.compositeItem.dayTimeConditions[0];
                            }
                            scope.maxSubgroups = newVal.maxSubGroups;
                            scope.maxSubgroupsUsed = scope.compositeItem.compositeItemGroups.length === scope.maxSubgroups;
                        }
                        
                    }
                );

  
                //fire event down to daytime objects when a primary dayTime setter sets applicable dates/times globally and raises event
                scope.$on('globalDaySetterChanged', function (event, disabledState) {
                    scope.$broadcast('enabledStatusChangeIssued', disabledState);
                });

                scope.$on('originalCompItemRestored', function (event, original){
                    scope.currentDetails = subgroupsService.getCurrentDetails(original, scope.userDc);
                });

                //internal functions 
                scope.addNewSubgroup = function () {    //add new subgroup to form 
                    var newSubgroup = subgroupsService.getNewSubgroup(scope.compositeItem.compositeItemGroups.length+1, scope.maxSubgroups);
                    scope.compositeItem.compositeItemGroups.push(newSubgroup);
                }
            }
        }
    }
})();