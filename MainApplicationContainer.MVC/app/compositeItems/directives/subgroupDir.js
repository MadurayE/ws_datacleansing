﻿(function () {
    "use strict";

    angular
        .module("app")
        .directive("ciDetailsSubgroup", ["$uibModal", "openModal", "userService", "$state", DetailsSubgroup])

    function DetailsSubgroup($uibModal, openModal, userService, $state) {
        return {
            restrict: 'AE',
            scope: {
                mySubgroup: '=',
                excludeVariants: '=',
                includeInactiveItems: '=',
                showOnlyMyDc: '=',
                myDc: '=',
                conditionDescription: '=',
                rewardDescription: '=',
                rewardCode: '=',
                detailsEditable: '=',
                itemCode: '=',
                currentDetails: '=',
                dayTimeDependent: '='

            },
            templateUrl: "app/compositeItems/directives/views/subgroup.html",
            link: function (scope, elem, attr) {
                //init scope
                scope.init = function () {
                    scope.formName = "subgroupForm" + scope.mySubgroup.subGroupNo;
                }
                scope.init();
                scope.details = scope.mySubgroup.compositeItemDetails;
                scope.formName = "subgroupForm" + scope.mySubgroup.subGroupNo;
                scope.newConditionValue = getConditionValue();
                scope.newRewardValue = getRewardValue();
                setDetailsRewardValue();

                setRewardDescriptionView();
                scope.showConditionValue = !scope.dayTimeDependent;

                //--- Watches
                scope.$watch( //watch for change of composite item type
                    'rewardDescription', function (newVal, oldVal) {
                        if (newVal) {
                            setRewardDescriptionView();
                        }

                    }
                );

                scope.$watch('dayTimeDependent', function (newVal, oldVal) {

                    scope.showConditionValue = !scope.dayTimeDependent;
                    if (scope.dayTimeDependent){
                        scope.newConditionValue = 1;
                    }

                });

                // ---Event handlers

                // Add button clicked. Add new details to subgroup - show item lookup modal
                scope.showItemLookup = function () {
                    //check if cookie exists
                    if (userService.cookieValid()) {
                        //define modal options
                        var options = {
                            templateUrl: "app/compositeItems/itemLookup/itemLookup.html",
                            size: "lookup",
                            controller: 'ItemLookupController as vm',
                            resolve: {
                                currentSubgroup: function () { return scope.mySubgroup.subGroupNo; },
                                primaryVendor: function () { return scope.mySubgroup.vendorDetails.vendorNumberCode; },
                                currentDetails: function () { return scope.currentDetails; }
                            }
                        }

                        //open modal
                        $uibModal.open(options).result.then(
                            function (selectedItems) {
                                if (selectedItems[0].newPrimaryVendor) {  //service returns an array with the first element being the "primay vendor object"
                                    scope.mySubgroup.vendorDetails = selectedItems[0].newPrimaryVendor;
                                }
                                selectedItems.splice(0, 1); //remove primary vendor object from array before adding details

                                //add selected Items to subgroup
                                Array.prototype.push.apply(scope.details, selectedItems);
                                Array.prototype.push.apply(scope.currentDetails, selectedItems);
                                swal("Add Details", "Item details have been added to the subgroup " + scope.mySubgroup.subGroupNo, "info");
                                scope.updateDetailsRewardValue();
                                scope.updateDetailsConditionValue();
                            },
                            function () { })
                    } else {
                        $state.go("home"); //return to home page to reload user cookies because no cookies present
                    };


                }

                //remove item detail from subgroup
                scope.removeDetail = function (index) {

                    openModal.confirm("Are you sure?", index, "This will be removed but changes will not be saved just yet").then(removeItem);
                    function removeItem(itemIndex) {
                        var codeToDelete = scope.details[itemIndex].code;
                        scope.details.splice(itemIndex, 1);
                        _.remove(scope.currentDetails, { code: codeToDelete });
                        if (scope.currentDetails.length === 0) {
                            scope.mySubgroup.vendorDetails = { primaryVendorCode: null, vendorNumberCode: null, vendorNumberName: null, vendorNumberId: null, vendorItemNumber: null };
                        }

                    }
                }

                //scope.$watch(function (scope) {
                //    return scope.newRewardValue;
                //}, function (newValue, oldValue) {
                //    if (newValue != oldValue && newValue != "") {
                //        alert("Changed");
                //    }
                //});

                // Condition Value entered - update sub group details
                scope.updateDetailsConditionValue = function () {
                    for (var i = 0; i < scope.details.length; i++) {
                        if (scope.details[i].businessUnitCode === scope.myDc) {
                            scope.details[i].promoQuantity = scope.newConditionValue;
                            scope.details[i].lastChgUserName = userService.getUserNameCookie();
                        }
                    }
                };

                // Reward Value entered - update sub group details
                scope.updateDetailsRewardValue = function () {
                    for (var i = 0; i < scope.details.length; i++) {
                        if (scope.details[i].businessUnitCode === scope.myDc) {
                            setRewardValue(scope.newRewardValue, i);
                            scope.details[i].lastChgUserName = userService.getUserNameCookie();
                        }
                    }
                };


                //internal functions --- should/can this go into a service

                function getConditionValue() {
                    if (scope.details.length > 0) {
                        var firstItemIndex = _.findIndex(scope.details, ['businessUnitCode', scope.myDc]) //find this DC's first entry
                        if (firstItemIndex > -1) {
                            return scope.details[firstItemIndex].promoQuantity;
                        } else {
                            return scope.dayTimeDependent=== true ? 1 :0;    //return 1 because condition value will be hidden for dayTimeDependent items, otherwise will fail validation
                        }
                    } else {
                        return scope.dayTimeDependent === true ? 1 : 0;    //return 1 because condition value will be hidden for dayTimeDependent items, otherwise will fail validation
                    }

                }

                // get the reward value used for current user dc
                function getRewardValue() {
                    if (scope.details.length > 0) {

                        var firstItemIndex = _.findIndex(scope.details, ['businessUnitCode', scope.myDc]) //find this DC's first entry
                        if (firstItemIndex > -1) { // item exists for this DC
                            switch (scope.rewardCode) { //check reward type code. Should have pushed this responsibility up to the service
                                case 1:
                                    return scope.details[firstItemIndex].totalPrice;
                                case 2:
                                    return scope.details[firstItemIndex].sellingPrice;
                                case 3:
                                    return scope.details[firstItemIndex].discountValue;
                                case 4:
                                    return scope.details[firstItemIndex].discountPercentage;
                                default:
                                    return 1;
                            }
                        } else { // no item for this DC
                            return scope.rewardCode === 5 ? 1 : 0; // return 1 if free product      
                        }

                    }
                    else {
                        return scope.rewardCode === 5 ? 1 : 0; // return 1 if free product   
                    }

                }


                // set the applicable reward value field for current dc
                function setRewardValue(value, index) {
                    if (scope.details.length > 0) {
                        switch (scope.rewardCode) { //check reward type code
                            case 1:
                                scope.details[index].totalPrice = value;
                                break;
                            case 2:
                                scope.details[index].sellingPrice = value;
                                break;
                            case 3:
                                scope.details[index].discountValue = value;
                                break;
                            case 4:
                                return scope.details[index].discountPercentage = value;
                                break;
                        }
                    }
                }

                // set item reward value to display other dc's applicable reward value (not bound to scope)
                function setDetailsRewardValue() {
                    if (scope.details.length > 0) {

                        for (var i = 0; i < scope.details.length; i++) {
                            switch (scope.rewardCode) { //check reward type code. Should have pushed this responsibility up to the service
                                case 1:
                                    scope.details[i].itemRewardValue = scope.details[i].totalPrice;
                                    break;
                                case 2:
                                    scope.details[i].itemRewardValue = scope.details[i].sellingPrice;
                                    break;
                                case 3:
                                    scope.details[i].itemRewardValue = scope.details[i].discountValue;
                                    break;
                                case 4:
                                    scope.details[i].itemRewardValue = scope.details[i].discountPercentage;
                                    break;
                            }
                        }
                    }
                }

                function setRewardDescriptionView() {
                    if (scope.rewardDescription === "Free Product") {  //move this into rules object service
                        if (scope.mySubgroup.subGroupNo === 1) {
                            scope.subGroupHeader = "Buy these Products";
                        } else {
                            scope.subGroupHeader = "Get these Products Free";
                        }

                        scope.showRewardValue = false;
                        scope.newRewardValue = 1;

                        //scope.rewardDescription = "";
                    } else {
                        scope.subGroupHeader = "Sub Group " + scope.mySubgroup.subGroupNo;
                        scope.showRewardValue = true;
                        scope.rewardSymbol = scope.rewardDescription.indexOf("Percentage") > -1 ? "%" : "R";
                    }

                        
                }
            }

        }

    }



})();