﻿(function(){
    "use strict";

    angular
        .module("app")
        .factory("subgroupsService", [subgroupsService])

    function subgroupsService() {

        var globalDaySetterSelected = false;

        function setGlobalDaySetterSelected(isSelected){
            globalDaySetterSelected = isSelected;
        }

        function getGlobalDaySetterSelected() {
            return globalDaySetterSelected;
        }
        //if item is day/time dependant and dayTime object is null, create new object
        function getDefaultDaytimeObject(itemCode, buCode) {
            var daytime = {};
            daytime.buCode = buCode;
            daytime.code = itemCode;
            daytime.codeDc = itemCode + " " + buCode;
            daytime.day1 = "FALSE";
            daytime.day2 = "FALSE";
            daytime.day3 = "FALSE";
            daytime.day4 = "FALSE";
            daytime.day5 = "FALSE";
            daytime.day6 = "FALSE";
            daytime.day7 = "FALSE";
            daytime.day8 = "FALSE";
            daytime.subGroupNo = 1;
            daytime.time1 = "FALSE";
            daytime.time1End = "00:00:00";
            daytime.time1Start = "00:00:00";
            daytime.time2 = "FALSE";
            daytime.time2End = "00:00:00";
            daytime.time2Start = "00:00:00";
            daytime.time3 = "FALSE";
            daytime.time3End = "00:00:00";
            daytime.time3Start = "00:00:00";
            daytime.time4 = "FALSE";
            daytime.time4End = "00:00:00";
            daytime.time4Start = "00:00:00";
            daytime.time5 = "FALSE";
            daytime.time5End = "00:00:00";
            daytime.time5Start = "00:00:00";
            daytime.time6 = "FALSE";
            daytime.time6End = "00:00:00";
            daytime.time6Start = "00:00:00";
            daytime.time7 = "FALSE";
            daytime.time7End = "00:00:00";
            daytime.time7Start = "00:00:00";
            daytime.time8 = "FALSE";
            daytime.time8End = "00:00:00";
            daytime.time8Start = "00:00:00";
            return daytime;
        }

        //create default subgroup object for when adding a new subgroup
        function getNewSubgroup(subgroupNo, maxGroups) {
            var newSubgroup = {};
            newSubgroup.subGroupNo = subgroupNo;
            newSubgroup.maxSubGroups = maxGroups;
            newSubgroup.vendorDetails = {};
            newSubgroup.compositeItemDetails = [];

            return newSubgroup;
        }

        function getCurrentDetails(compositeItem, currentDC) {
            var currentDetails = [];
            for (var i = 0; i < compositeItem.compositeItemGroups.length; i++) {
                for (var j = 0; j < compositeItem.compositeItemGroups[i].compositeItemDetails.length; j++) {
                    var bu = compositeItem.compositeItemGroups[i].compositeItemDetails[j].businessUnitCode;
                    if (bu === currentDC) {
                        currentDetails.push(compositeItem.compositeItemGroups[i].compositeItemDetails[j]);
                    }
                }
            }
            return currentDetails;
        }

        return {
            getDefaultDaytimeObject: getDefaultDaytimeObject,
            getNewSubgroup: getNewSubgroup,
            setGlobalDaySetterSelected: setGlobalDaySetterSelected,
            getGlobalDaySetterSelected: getGlobalDaySetterSelected,
            getCurrentDetails: getCurrentDetails
        }
    }

})();