﻿(function(){
    "use strict";

    angular
        .module("app")
        .directive("dayTimeSelector", ["$filter","subgroupsService",dayTimeSelector])

    function dayTimeSelector($filter,subgroupsService) {

        return {
            restrict: 'E',
            scope: {
                dayModel: "=", 
                timeModel:"=",    
                globalDaySetter: "=",
                actualStartTime: "=",
                actualEndTime: "=",
            },
            replace:'true',
            templateUrl: "app/compositeItems/directives/views/dayTimeSelector.html",

            link: function (scope, elem, attr) {
                //check parameters for nulls
                if (scope.dayModel === null || scope.dayModel==="") { scope.dayModel = "FALSE" }
                if (scope.timeModel === null || scope.timeModel === "") { scope.timeModel = "FALSE" }
                if (scope.actualStartTime === null || scope.actualStartTime === "") { scope.actualStartTime = "00:00:00" }
                if (scope.actualEndTime === null || scope.actualEndTime === "") { scope.actualEndTime = "00:00:00" }

                //convert time strings to H,m,s
                scope.parseTimes = function (startT, endT) {
                    var ret = startT.split(":").concat(endT.split(":"));
                    return ret;
                }


                scope.dayLabel = attr.dayLabel;  //set Label value ie, Monday, Tuesday etc
                scope.timesModel = scope.parseTimes(scope.actualStartTime, scope.actualEndTime);  //extract H,m,s from time strings

                //use parsed time strings to set timepicker values
                scope.startTime = new Date();
                scope.startTime.setHours(scope.timesModel[0]);
                scope.startTime.setMinutes(scope.timesModel[1]);
                scope.startTime.setSeconds(scope.timesModel[2]);
                scope.endTime = new Date();
                scope.endTime.setHours(scope.timesModel[3]);
                scope.endTime.setMinutes(scope.timesModel[4]);
                scope.endTime.setSeconds(scope.timesModel[5]);

                  //variable to control whether control is active or not
                if (scope.globalDaySetter) {
                    subgroupsService.setGlobalDaySetterSelected(scope.dayModel === "TRUE");
                }
                scope.disableControl = subgroupsService.getGlobalDaySetterSelected() && !scope.globalDaySetter;

                if (scope.timeModel === null) { scope.timeModel = 'FALSE' }  //default 'all day' field if db val is null
                scope.dayBoxChanged = function () {
                    //when daybox is changed raise event if this is a Global Day Setter control ie 'Everyday'
                    if (scope.globalDaySetter) {
                        scope.$emit('globalDaySetterChanged', scope.dayModel === "TRUE");
                    }
                }

                // listen for event issuing change to status to disable/re-enable control
                scope.$on('enabledStatusChangeIssued', function (event, disabledState) {
                    if (!scope.globalDaySetter) {
                        scope.disableControl = disabledState;
                    }
                })

                //set new start and end time when timepicker control changed
                scope.updateTimesModel = function (varToUpdate) {
                    if (varToUpdate === 'start') {
                        scope.timesModel[0] = $filter('date')(scope.startTime, 'HH');
                        scope.timesModel[1] = $filter('date')(scope.startTime, 'mm');
                        scope.timesModel[2] = $filter('date')(scope.startTime, 'ss');
                        scope.actualStartTime = scope.timesModel[0] + ":" + scope.timesModel[1] + ":" + scope.timesModel[2]

                    } else {
                        scope.timesModel[3] = $filter('date')(scope.endTime, 'HH');
                        scope.timesModel[4] = $filter('date')(scope.endTime, 'mm');
                        scope.timesModel[5] = $filter('date')(scope.endTime, 'ss');
                        scope.actualEndTime = scope.timesModel[3] + ":" + scope.timesModel[4] + ":" + scope.timesModel[5]
                    }
                }

            }
        }
    }

})();