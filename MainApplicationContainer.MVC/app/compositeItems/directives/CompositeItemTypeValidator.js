﻿(function () {

    "use strict";

    angular
        .module("app")
        .directive("spCiTypeInactive", [spCiTypeInactive])

    function spCiTypeInactive() {
        return {
            restrict: "A",

            require: "ngModel",

            link: function (scope, elem, attr, ngModel) {
                ngModel.$validators.spCiTypeInactive = function (modelValue) {
                    if (modelValue) {
                        return modelValue.activeCode === "Y";
                    } else {
                        return false;
                    }
                }
            }
        }
    }
})();