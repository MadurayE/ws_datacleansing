﻿(function (){
    "use strict";

    angular
        .module("app")
        .factory("itemLookupService", ["appConfig", "$resource","userService","$q", "$log", itemLookupService])

    function itemLookupService(appConfig, $resource, userService, $q, $log)
    {
        var itemDataService = $resource(appConfig.SERVERPATH + "/api/NormalItems/:code", null,
                {
                    'searchFor': { method: "get", isArray: true }
                });
        //create composite item detail from normal item
        function createItemDetailFromNormal(normalItem, currentSubgroup) {
            var newDate = new Date();
            var cItemDetail = {}
            // set item details 
            cItemDetail.memberId = 0;
            cItemDetail.compositeCode = 0;// normalItem.nationalItemReference;
            cItemDetail.itemNumber = normalItem.memberId;
            cItemDetail.businessUnitCode = normalItem.businessUnitCode;
            cItemDetail.name = normalItem.productDescription;
            cItemDetail.code = ""; //normalItem.nationalItemReference + " " + normalItem.businessUnitCode + " "
                                //+ normalItem.memberId;
            cItemDetail.itemTypeCode = "N";
            cItemDetail.nationalItemNumberCode = normalItem.businessUnitCode + " " + normalItem.memberId;
            cItemDetail.nationalItemNumberName = normalItem.productDescription;
            cItemDetail.itemSize = normalItem.itemSize;
            cItemDetail.itemPack = normalItem.storeRetailUnit;
            cItemDetail.freeProduct = "N";
            cItemDetail.itemDistributionMethodName = normalItem.itemDistributionMethodName;
            cItemDetail.recipeId = null;
            cItemDetail.recipeName = null;
            cItemDetail.cusEAN = normalItem.consumerEANCode;
            cItemDetail.promoQuantity = 0;
            cItemDetail.sellingPrice = 0;
            cItemDetail.discountValue = 0;
            cItemDetail.discountPercentage = 0;
            cItemDetail.totalPrice = 0
            cItemDetail.deletedOnErp = normalItem.deletedOnErp;
            cItemDetail.variantMasterIndicator = normalItem.variantMasterCode;
            cItemDetail.variantMasterCode = normalItem.itemVariantMasterCode;
            cItemDetail.subGroupNo = currentSubgroup;
            cItemDetail.activeCode = normalItem.activeCode;
            cItemDetail.activeName = "Yes";
            cItemDetail.enterDateTime = newDate;
            cItemDetail.enterUserName = userService.getUserNameCookie();
            cItemDetail.lastChgDateTime = newDate;
            cItemDetail.lastChgUserName = userService.getUserNameCookie();
            cItemDetail.msrepltranversion = "d93ad3ce-e911-4d4b-8006-f016daec1d43";
            cItemDetail.versionName = "Version_1";
            cItemDetail.versionNumber = 1;
            cItemDetail.changeTrackingMask = 0;
            cItemDetail.enterVersionNumber = 1;
            cItemDetail.versionFlag = "Promotion_Phase_3a";
            //add item to array
            return cItemDetail;
        }

        //create composite item detail from service department item
        function createItemDetailFromServiceDept(serviceDeptItem, currentSubgroup) {
             var newDate = new Date();
            var cItemDetail = {}
            // set item details 
            cItemDetail.memberId = 0; //serviceDeptItem.nationalItemCode;
            cItemDetail.activeCode = serviceDeptItem.activeCode;
            cItemDetail.businessUnitCode = serviceDeptItem.businessUnitCode;
            cItemDetail.code = "", //serviceDeptItem.nationalItemCode + " " + serviceDeptItem.businessUnitCode + " "
                                //+ serviceDeptItem.sdCiShortCode;
            cItemDetail.discountPercentage = 0;
            cItemDetail.discountValue = 0;
            cItemDetail.enterDateTime = newDate;
            cItemDetail.enterUserName = userService.getUserNameCookie();
            cItemDetail.freeProduct = "N"; //Att: look at the rules here
            cItemDetail.itemTypeCode = "S";
            cItemDetail.itemDistributionMethodName = "";
            cItemDetail.itemNumber = serviceDeptItem.sdCiShortCode;
            cItemDetail.itemPack = "";
            cItemDetail.itemSize = serviceDeptItem.size;
            cItemDetail.lastChgDateTime = newDate;
            cItemDetail.lastChgUserName = userService.getUserNameCookie();
            cItemDetail.name = serviceDeptItem.name;
            cItemDetail.nationalItemNumberCode = serviceDeptItem.businessUnitCode + " " + serviceDeptItem.sdCiShortCode;
            cItemDetail.nationalItemNumberName = serviceDeptItem.name;
            cItemDetail.promoQuantity = 0;
            cItemDetail.sellingPrice = 0;
            cItemDetail.totalPrice = 0
            cItemDetail.variantMasterCode = "";
            cItemDetail.subGroupNo = currentSubgroup;
            cItemDetail.recipeId = serviceDeptItem.recipeCode;
            cItemDetail.recipeName = serviceDeptItem.recipeName;
            cItemDetail.variantMasterIndicator = "";
            cItemDetail.compositeCode = 0;//serviceDeptItem.nationalItemCode;
            cItemDetail.cusEAN = serviceDeptItem.serviceDeptCusEan;
            cItemDetail.deletedOnErp = "N";
            cItemDetail.msrepltranversion = "d93ad3ce-e911-4d4b-8006-f016daec1d43";
            cItemDetail.versionName = "Version_1";
            cItemDetail.versionNumber = 1;
            cItemDetail.changeTrackingMask = 0;
            cItemDetail.enterVersionNumber = 1;
            cItemDetail.versionFlag = "Promotion_Phase_3a";

            return cItemDetail;
        } appConfig


        //check which of the search results already exist on the composite item
        function markDuplicatesBetween(searchDetails, currentDetails, isNormal) {
            var searchFields = "";

            for (var i = 0; i < currentDetails.length; i++) {
                if (isNormal){
                    searchFields = currentDetails[i].nationalItemNumberCode;
                } else {
                    searchFields = currentDetails[i].businessUnitCode + " " + currentDetails[i].compositeCode;
                }
                var dup = _.findIndex(searchDetails, ['code', searchFields]);
                if (dup > -1) {
                    searchDetails[dup].isDuplicate = true;
                }
            }
        }

        function getVariantsData(masterCode) {
            return itemDataService.searchFor({ master: masterCode, bu: userService.dcShortCodeCookie() }).$promise;

        }

        function resolvePromises(promiseArray, currentSubgroup, primaryVendor, currentDetails) {
            var deferred = $q.defer();
            var retArray = [];
            var cItemDetail={};
            var primaryVendorObj = { "nonPrimaryVendorFlag": false, "newPrimaryVendor": null}; //use this object to hold primary vendor details
            $q.all(promiseArray).then(function (variantsData) {
                for (var i = 0; i < variantsData.length; i++) {
                    markDuplicatesBetween(variantsData[i], currentDetails, true);
                    var vArr = variantsData[i];
                    for (var j = 0; j < vArr.length; j++) {
                        if (!vArr[j].isDuplicate) {
                            if (vArr[j].vendorCode != primaryVendor && primaryVendor) { primaryVendorObj.nonPrimaryVendorFlag = true; }
                            if (!primaryVendor && (i + j) === 0) { // if there's no primary vendor and this is the first item being added, then set new primary vendor
                                primaryVendorObj.newPrimaryVendor = {};
                                primaryVendorObj.newPrimaryVendor.vendorNumberCode = vArr[j].vendorCode;
                            }
                            cItemDetail = createItemDetailFromNormal(vArr[j], currentSubgroup);
                            retArray.push(cItemDetail);
                        }
                     
                    }
                }
                retArray.splice(0, 0, primaryVendorObj); //add primary vendor object to the return array
                deferred.resolve(retArray);
            });
            return deferred.promise;
        }

        // add normal items - get master and variants from server
        function addNormalItems(masterCodes, currentSubgroup, primaryVendor, currentDetails) {
            var promiseArray = [];
            for (var i=0; i<masterCodes.length; i++){
                //call service
                if (!masterCodes[i].masterCode || masterCodes[i].masterCode===0){
                    $log.warn("Missing Master Code for: " + masterCodes[i].description + " at Row: " + masterCodes[i].row+1);
                } else {
                    var variants = getVariantsData(masterCodes[i].masterCode);
                    promiseArray.push(variants);
                }

            } 
            if (promiseArray.length > 0) {
                return resolvePromises(promiseArray, currentSubgroup, primaryVendor, currentDetails);
            } else {
                var err = $q.defer();
                var errData = { errorEncountered: { message: "No valid master codes were found. These have been logged to the console for Support to look at." } };
                err.resolve(errData);
                return err.promise;
            }
            
        }

        function processNormalItemAdd(masterCodes, currentSubgroup, primaryVendor, currentDetails){
            return addNormalItems(masterCodes, currentSubgroup, primaryVendor, currentDetails);
        }

        //add service dept items
        function addServiceDeptItems(selectedRows, searchResults, currentSubgroup) {
            var cItemDetail = {};
            var retArray = []; var position = -1;
            for (var i = 0; i < selectedRows.length ; i++) {
                position = selectedRows[i];
                cItemDetail = createItemDetailFromServiceDept(searchResults[position], currentSubgroup);
                //add item to array
                retArray.push(cItemDetail);
            }
            var primaryVendorObj = { "nonPrimaryVendorFlag": false, "newPrimaryVendor": null };
            retArray.splice(0, 0, primaryVendorObj); //add primary vendor object to the return array - FinishAddingFunction expects this
            return retArray;
        }

        return {
            itemDataService: itemDataService,
            serviceItemDataService: $resource(appConfig.SERVERPATH + "/api/ServiceDepartmentItems/:code", null,
                {
                    'searchFor':{method:"get", isArray:true}
                }),
            createItemDetailFromNormal: createItemDetailFromNormal,
            createItemDetailFromServiceDept: createItemDetailFromServiceDept,
            markDuplicatesBetween: markDuplicatesBetween,
            addServiceDeptItems: addServiceDeptItems,
            processNormalItemAdd:processNormalItemAdd

        }
       
    }


})();