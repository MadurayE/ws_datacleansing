﻿(function(){
    "use strict";

    angular
        .module("app")
        .controller("ItemLookupController", 
            ["$uibModalInstance","itemLookupService","currentSubgroup","primaryVendor","userService","openModal",
            "currentDetails","$q","usSpinnerService", ItemLookupController])

    function ItemLookupController($uibModalInstance, itemLookupService, currentSubgroup, primaryVendor, userService, openModal, currentDetails, $q, usSpinnerService) {
        //initialise scope defaults
        var vm = this;
        vm.currentDetails = currentDetails;
        vm.searchTarget = "Normal";
        vm.selectedRows = [];  //use this to hold pointers to selected items of search results
        vm.selectedItems = [];   //use this to hold item details to return to subgroup
        vm.masterCodes = [];    // use this to store the product master codes whose variants will be added
        vm.variantsAlreadyAdded = false;
        vm.primaryVendor = primaryVendor;
        vm.currentDc = userService.dcShortCodeCookie();
        //end initialise

        //clear search results
        vm.resetSearch =
            function () {
                vm.itemDetails = null;
            }

        //execute search function
        vm.searchItems =
            function () {
                usSpinnerService.spin('itemSearch');
                vm.itemDetails = null;  //set defaults for new search
                vm.selectedRows = [];

                if (vm.itemSearch != "" || vm.searchText != "") {
                    if (vm.searchTarget === "Normal") {  //normal Item search
                        itemLookupService.itemDataService.searchFor({ name: vm.searchText, item: vm.itemSearch, Exclude: false, bu:vm.currentDc },
                            handleSuccess, handleFailure);
                    } else {//service department item search
                        itemLookupService.serviceItemDataService.searchFor({ name: vm.searchText, item: vm.itemSearch, Exclude: false, bu:vm.currentDc },
                                handleSuccess, handleFailure);

                    }
                }

            }
        // end execute search

        // when checkbox is clicked, add or remove item from pointer array
        // if its a variant dont add to pointer array but to master code array. 
        // master code array will be used later to add all variants
        vm.selectItem=
            function (checkbox, selectedRow) {
                if (checkbox === "selected") {
                    if (vm.searchTarget != "Normal") {
                        vm.selectedRows.push(selectedRow); //add item pointer to array if it's a service dept item
                    } else {
                        //save master code so variants can be added later
                        vm.masterCodes.push({ row: selectedRow, masterCode: vm.itemDetails[selectedRow].itemVariantMasterCode, description: vm.itemDetails[selectedRow].productDescription });
                    }
                }
                else { //remove pointer from array if item is master or service dept item
                    if (vm.searchTarget != "Normal") {
                        vm.selectedRows.splice(vm.selectedRows.indexOf(selectedRow), 1);
                    } else {
                        //remove saved master code entry
                       vm.masterCodes.splice(_.findIndex(vm.masterCodes, ['row', selectedRow]),1);
                    }
                }
            }
        //end selectItem function

        //---- Event handlers

        // when Add Selected Items button is clicked
        vm.addSelectedItems = 
            function () {
                var position = -1;

                var normalItemResults=[];

                if (vm.searchTarget === "Normal") {
                    //call normal search resolution
                    if (vm.masterCodes.length > 0) {
                        vm.masterCodes = _.uniqBy(vm.masterCodes, 'masterCode');
                        itemLookupService.processNormalItemAdd(vm.masterCodes, currentSubgroup, vm.primaryVendor, vm.currentDetails).then(finishAddingItems);
                    }
                } else {
                    if (vm.selectedRows.length > 0) {
                        vm.selectedItems = itemLookupService.addServiceDeptItems(vm.selectedRows, vm.itemDetails, currentSubgroup);
                        finishAddingItems(vm.selectedItems);
                    }
                }
            
            }

        // functions

        //call back for add items
        var finishAddingItems = function (itemsToBeAdded) {
            if (!itemsToBeAdded.errorEncountered) {
                var primaryVendorObj = itemsToBeAdded[0]; //service returns an array with the first element being the "primary vendor object"
                if (primaryVendorObj.nonPrimaryVendorFlag) {
                    openModal.confirm("Add Composite Item Detail", itemsToBeAdded, "Items selected do not match the primary vendor of the group. Continue?").then(
                         closeModalAndSave
                          );
                } else {
                    closeModalAndSave(itemsToBeAdded);
                }
            } else {
                swal("Could Not Add Items", itemsToBeAdded.errorEncountered.message, "error");
            }

            function closeModalAndSave(items) {
                $uibModalInstance.close(items)
            }
        }

        // search items service success callback
        var handleSuccess = function (data) {
            usSpinnerService.stop('itemSearch');
            if (data.length === 0) {
                swal("No results found", "We did not find records matching your search criteria", "warning");
            } else {
                vm.itemDetails = data;
                if (vm.itemDetails) {
                    itemLookupService.markDuplicatesBetween(vm.itemDetails, vm.currentDetails, vm.searchTarget==="Normal");
                }
            }

        }
        // search items service error callback
        var handleFailure = function (error) {
            usSpinnerService.stop('itemSearch');
            openModal.swalErr("Oops, could complete search", error.statusText, error.data.message);

        }
    }
})();