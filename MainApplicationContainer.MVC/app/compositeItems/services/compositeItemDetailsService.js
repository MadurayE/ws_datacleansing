﻿(function () {
    "use strict";

    angular
        .module("app")
        .factory("CompositeItemDetailsService",["appConfig", "$resource",CompositeItemDetailsService])

    function CompositeItemDetailsService(appConfig,$resource) {
        return {
            itemDetailsService: $resource(appConfig.SERVERPATH + "api/compositeItemDetails", null,
                {
                    'getByItemCode': { method: 'Get', isArray: true }
                }

            )
        }
    }
    
})();