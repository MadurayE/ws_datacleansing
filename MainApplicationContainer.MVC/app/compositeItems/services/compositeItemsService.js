﻿(function () {
    "use strict";

    angular
        .module("common.services")
        .factory("CompositeItemsService",
        ["appConfig", "$resource","userService", CompositeItemsService]);


    function CompositeItemsService(appConfig, $resource, userService) {
        var getDataservice =  $resource(appConfig.SERVERPATH + "api/compositeItems/:id", null,
            {
                'searchFor': { method: 'GET', isArray: true },
                'update':{method:'PUT', withCredentials:true }
            });
        var editPropertiesService = $resource(appConfig.SERVERPATH + "api/compositeItems/:id/editableProperties");
        
        function getCurrentDetails(compositeItem) {
            var currentDetails = [];
            var currentDC = userService.dcShortCodeCookie;
            for (var i = 0; i <= compositeItem.compositeItemGroups.length; i++) {
                for (var j = 0; j <= compositeItem.compositeItemGroups[i].compositeItemDetails.length; j++) {
                    if (compositeItem.compositeItemGroups[i].compositeItemDetails[j].businessUnitCode === currentDC) {
                        currentDetails.push(compositeItem.compositeItemGroups[i].compositeItemDetails[j]);
                    }
                }
            }
            return currentDetails;
        }

        return {
            dataService: getDataservice,    //resource for composite items
            itemEditProperties: editPropertiesService,   //resource for composite items editable properties
            getCurrentDetails:getCurrentDetails //get list item details for this DC from composite group

        }
       
    }
})();