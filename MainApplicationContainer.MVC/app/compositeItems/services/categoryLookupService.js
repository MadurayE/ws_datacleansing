﻿(function(){

    "use strict";

    angular
        .module("app")
        .factory("categoryLookupService", ["appConfig","$resource", categoryLookupService])

    function categoryLookupService(appConfig,$resource) {

        var planningCategoryLookup = $resource(appConfig.SERVERPATH + "/api/planningCategories");
        var retailCategoryLookup = $resource(appConfig.SERVERPATH + "/api/retailCategories");

        return {
            planningCategoryLookup: planningCategoryLookup,
            retailCategoryLookup: retailCategoryLookup
        }
    }

    

})();