﻿(function () {
    'use strict';

    angular.module('appCore', [
        // Angular modules 
        "ui.router",
        "ui.bootstrap",
        "ngMessages",
        "ngCookies",
        "ngAnimate",
        "ui.grid",
        "ngSanitize",
        "ui.select",

        // Custom modules 
        "common.services", 
        "common.directives", 

        // 3rd Party Modules
        "angular-loading-bar",
        "angularSpinner"
    ]);
})();