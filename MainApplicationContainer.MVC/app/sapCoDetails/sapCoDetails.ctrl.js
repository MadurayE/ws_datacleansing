﻿(function () {
    'use strict';

    angular
        .module('sapCoDetails')
        .controller('sapCoDetailsController', sapCoDetailsController);

    sapCoDetailsController.$inject = []; 

    function sapCoDetailsController() {
        /* jshint validthis:true */
        var vm = this;
        vm.title = 'sapCoDetails';

        activate();

        function activate() { }
    }
})();
