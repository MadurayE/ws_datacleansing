﻿(function () {
    'use strict';

    angular.module('sapCoDetails', [
        // Angular modules 

        // Custom modules 
        "appCore"
        // 3rd Party Modules
        
    ])

    .config(["$stateProvider", "$urlRouterProvider", "$locationProvider", sapCoDetailsConfig])

    function sapCoDetailsConfig($stateProvider, $urlRouterProvider, $locationProvider) {
        $stateProvider
        .state("sapCoDetails",
        {
            url: "/sapCoDetails",
            templateUrl: "app/sapCoDetails/sapCoDetails.html",
            controller: "sapCoDetailsController as vm"
        });
    }
})();