﻿(function () {
    "use strict";

    angular
        .module("common.directives")
        .directive("alerts", ["alertsService", alerts]);

    function alerts(alertsService) {
        return {
            restrict: "AE",
            templateUrl: "app/common/alerts/alerts.html",
            scope: true,
            controller: ["$scope", function($scope){
               $scope.closeAlert = function (alert) {
                    alertsService.removeAlert(alert);
                };
            }],
            link: function(scope){
                scope.raisedAlerts = alertsService.raisedAlerts;
            }
        }
    };

})();