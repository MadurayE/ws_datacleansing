﻿(function () {
    "use strict";

    angular
        .module("common.directives")
        .factory("alertsService", ["$timeout", alertsService]);


    function alertsService($timeout) {

        var raisedAlerts = [];

        function raiseAlert(type, message) {
            var alert = { type: type, message: message };
            raisedAlerts.push(alert);
            $timeout(function () {
                removeAlert(alert);
            }, 10000);

        }

        function raiseWarning(message) {
            raiseAlert("warning", message);
        }

        function raiseSuccess(message) {
            raiseAlert("success", message);
        }
        function raiseInfo(message) {
            raiseAlert("info", message);
        }
        function raiseDanger(message) {
            raiseAlert("danger", message);
        }

        function removeAlert(alert) {
            for (var i = 0; i < raisedAlerts.length; i++) {
                if (raisedAlerts[i] === alert) {
                    raisedAlerts.splice(i, 1);
                    break;
                }
            }
        }

        function removeAllAlerts() {
            var count = raisedAlerts.length;
            for (var i = 0; i < count ; i++) {
                    raisedAlerts.splice(0, 1);     
            }
        }

        return {
            raisedAlerts: raisedAlerts,
            raiseAlert: raiseAlert,
            raiseWarning: raiseWarning,
            raiseSuccess: raiseSuccess,
            raiseInfo: raiseInfo,
            raiseDanger: raiseDanger,
            removeAlert: removeAlert,
            removeAllAlerts: removeAllAlerts
        }

    }

})();