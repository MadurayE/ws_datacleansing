﻿(function () {
    "use strict";

    angular
		.module("common.services",
					["ngResource","ngCookies"])
		.constant("appSettings",
		{
            //locals
		    localMockServerPath: "http://localhost:12386/",
            localApiServerPath: "http://localhost:51880/",

            //DEV
            devServerPath: "http://coswebdev01:5001/",
            devCoreUserServerPath: "http://coswebdev01:83/",
            devCoreMDMServerPath: "http://coswebdev01.spar.net:84",

            //QA
            qaServerPath: "http://cosapiqa1:5001/",
            qaCoreServerPath: "http://cosapiqa1/",

            //PROD
		    prodServerPath: "LiveServerPathNotSet",

            //TARGET
            targetServer:"DEV"                     //options are DEV, QA or PROD

		});

    angular
        .module("common.services")
        .factory("appConfig", ["appSettings", AppConfig])

    function AppConfig(appSettings) {

        var serverPath = getServerPath();

        function getServerPath(){
            switch(appSettings.targetServer) {
                case "DEV":
                    return appSettings.devServerPath;
                case "QA":
                    return appSettings.qaServerPath;
                case "PROD":
                    return appSettings.prodServerPath;
                default:
                    return "ERROR: TARGET SERVER NOT CONFIGURED";
            }
        }

        var coreMDMServerPath = getCoreMDMServerPath();

        function getCoreMDMServerPath() {
            switch (appSettings.targetServer) {
                case "DEV":
                    return appSettings.devCoreMDMServerPath;
                case "QA":
                    return appSettings.qaCoreMDMServerPath;
                case "PROD":
                    return appSettings.prodCoreMDMServerPath;
                default:
                    return "ERROR: TARGET SERVER NOT CONFIGURED";
            }
        }

        var coreUserServerPath = getCoreUserServerPath();

        function getCoreUserServerPath() {
            switch (appSettings.targetServer) {
                case "DEV":
                    return appSettings.devCoreUserServerPath;
                case "QA":
                    return appSettings.qaCoreUserServerPath;
                case "PROD":
                    return appSettings.prodCoreUserServerPath;
                default:
                    return "ERROR: TARGET SERVER NOT CONFIGURED";
            }
        }

        return {
            SERVERPATH: serverPath,
            COREMDMSERVERPATH: coreMDMServerPath,
            COREUSERSERVERPATH: coreUserServerPath
        }

    }

    angular.module("common.directives", ["ngMessages", "common.services"]);
}());