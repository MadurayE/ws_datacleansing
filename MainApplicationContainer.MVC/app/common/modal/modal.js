﻿(function () {
    "use strict";

    angular
        .module("common.services")
        .factory("openModal", ["$uibModal", openModal]);


    function openModal($uibModal) {
        function confirm(title, item, message) {
            var options = { 
                animation:true,
                templateUrl: "app/common/modal/modal.html",
                controller: function () {
                    this.title = title;
                    this.item = item;
                    this.message = message;
                },
                controllerAs: "model"
               
            }
            return $uibModal.open(options).result;
        };

        function largeContainer(title, item, template) {
            var options = {
                templateUrl: template,
                size: "lg",
               
                

            }
            return $uibModal.open(options).result;

        }

        function swalErr(title, statustxt, detailsTxt) {
            swal({ title: title, text: detailsTxt,type:"error", html: true });
            //swal({ title: title, text: "<strong>Error: " + statustxt + "</strong><br/><samp>" + detailsTxt + "</samp>", html: true });
        }

        function swalSuccess(title, retObj) {
            if (retObj.partialSuccess===false){
                swal({ title: title, text: retObj.message, type: "success", html: true });
            } else {
                swal({ title: title, text: retObj.message, type: "info", html: true });
            }

            //swal({ title: title, text: "<strong>Error: " + statustxt + "</strong><br/><samp>" + detailsTxt + "</samp>", html: true });
        }

          return {
              confirm: confirm,
              largeContainer: largeContainer,
              swalErr: swalErr,
              swalSuccess: swalSuccess

          }
    }
})();

