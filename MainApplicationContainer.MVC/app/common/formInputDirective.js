﻿(function () {
    "use strict";

    angular
        .module("common.directives")
        .directive("formInput", ["$compile", formInput])

    function formInput($compile) {

        var decorateElement = function (element, form) {
            var input = element.querySelector("input, textarea, select");
            var type = input.getAttribute("type");
            if (type !== "checkbox" && type !== "radio") {
                input.classList.add("form-control");
            }
            if (type === "text") { input.classList.add("input-md");}

            var label = element.querySelector("label");
            label.classList.add("control-label");

            element.classList.add("form-group");
            var name = input.getAttribute("name");
        
            return name;
        }

        var addNgMessages = function (form, element, name, scope,fname) {
            var messages = "<div class='help-block has-error '>" +
                            "<div ng-messages='" + form.$name + "." + name + ".$error' role='alert'> " +
                                "<div ng-messages-include='app/common/templates/ngMsges.html' ng-init='fieldName=\""+fname+"\"'></div>"+
                            "</div>"
            //+"<pre>{{citForm.citName.$error | json}}</pre>"
                             +"</div>";

                            element.append($compile(messages)(scope));
        }
        
        return {
            restrict: 'A',
            require:'^form',
            link: function (scope, element, attributes, form) {

                //element.removeAttr("form-input"); element.removeAttr("data-form-input");
                var name = decorateElement(element[0], form);
                var fieldName = attributes.fieldName;
                if (!fieldName) { fieldName = "Field";}
                addNgMessages(form, element, name, scope, fieldName);
                //element.attr("ng-class", "{'has-error':" + form.$name + "." + name + ".$invalid}");

                scope.$watch(form.$name + '.' + name + '.$invalid', function (newval) {
                    element.toggleClass('has-error', !!newval);
                });

                //
                //$compile(element)(scope);
            }
        }
    }

})();