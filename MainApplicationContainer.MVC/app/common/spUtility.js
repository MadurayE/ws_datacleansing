﻿(function(){
    "use strict";

    angular
        .module("common.services")
        .factory("spUtility",["$log", spUtility])

        function spUtility($log){
            function handleServiceError(error, message) {
                if (error.data) {
                    swal(message, error.data.message, "error");
                } else {
                    if (error.status < 0) {
                        swal(message, "We can't seem to connect to the server: ", "error");
                    }
                   
                }
                $log.error("Error: ", error);
            }

            return {
                handleServiceError:handleServiceError
            }
        }
})();