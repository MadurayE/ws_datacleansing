﻿(function () {
    "use strict";

    angular.module("common.directives")
    .directive("ngEnter", [ngEnter])

    function ngEnter() {
        return {
            link: function (scope, elements, attrs) {
                elements.bind('keydown keypress', function (event) {
                    if (event.which === 13) {
                        scope.$apply(function () {
                            scope.$eval(attrs.ngEnter);
                        });
                        event.preventDefault();
                    }
                });
            }
        }
    }
})();

