﻿(function () {
    "use strict";

    angular
        .module("common.services")
        .factory("userService", ["$resource", "appConfig", "$cookies", UserService])

    function UserService($resource, appConfig, $cookies) {

        var getUserResource = $resource(appConfig.COREUSERSERVERPATH + "spardata/api/v1/SPAR/user/:username", null,
            {
                'get': { method: 'GET', withCredentials: true }
            });
       

        var getUserAuthResource = $resource(appConfig.SERVERPATH + "api/Authorisation/:username", null,
        {
            'get': { method: 'GET', withCredentials: true }
        });




        function getDCShortCodeCookie() {
            return $cookies.get('dcShortCode');
        }

        function getUserNameCookie() {
            return $cookies.get('currentUserName');
        }

        var getUserNameResource = $resource("/api/user");
        function getCurrentUserName() {
            //get user's login name in order to make AD service call
            return getUserNameResource.get().$promise;
        }


        function setDcShortCodeCookie(shortCode) {
            if ($cookies.get('dcShortCode')) {
                $cookies.remove('dcShortCode');
            };

            $cookies.put('dcShortCode', shortCode);
        }

        function cookieValid() {
            return $cookies.get('dcShortCode')?true:false;
        }

        return {
            userResource: getUserResource,
            userAuthResource: getUserAuthResource,
            userNameResource: getUserNameResource,
            dcShortCodeCookie: getDCShortCodeCookie,
            currentUserName: getCurrentUserName,
            setDcShortCodeCookie: setDcShortCodeCookie,
            getUserNameCookie: getUserNameCookie,
            cookieValid: cookieValid
        }
    }


})();