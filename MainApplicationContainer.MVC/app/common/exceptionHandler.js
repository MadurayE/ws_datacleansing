﻿(function(){
    "use strict";
    
    angular
        .module("common.services")

        .config(["$provide", function ($provide) {

            $provide.decorator("$exceptionHandler",["$delegate", "$injector", function ($delegate, $injector) {
                return function(exception, cause){
                    $delegate(exception, cause);
                    swal("Oops, something went wrong unexpectedly", exception.message, "error");
                };
            }]);
        }
        ]);

})();